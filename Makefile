BINARY=dd-downtime

VERSION=`git describe --tags`

LDFLAGS=-ldflags "-w -s -X main.build=${VERSION} "

build:
	go build ${LDFLAGS} -o ${BINARY}

build-release:
	go build ${LDFLAGS} -o ${BINARY}-${VERSION}
	env GOOS=linux GOARCH=amd64 go build ${LDFLAGS} -o ${BINARY}-${VERSION}-linux

install:
	go install ${LDFLAGS}

clean:
	rm -vrf ${BINARY}.*
#	if [ -f ${BINARY}-* ] ; then rm ${BINARY}-* ; fi
#	if [ -f ${BINARY}-${VERSION}-linux ] ; then rm ${BINARY}-${VERSION}-linux ; fi
