package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/ardanlabs/conf"
	"github.com/fatih/color"
	"github.com/pkg/errors"
)

// build is the git version of our app
var build = "v0.0d"

// used this just because most everbody has the DD_API/APP already set
var appname = "DD"

func main() {
	log := log.New(os.Stdout, fmt.Sprintf("%s : ", appname), log.LstdFlags|log.Lmicroseconds|log.Lshortfile)

	if err := run(log); err != nil {
		log.Println("main: error:", err)
		os.Exit(1)
	}
}
func run(log *log.Logger) error {
	var cfg struct {
		conf.Version
		API      string `conf:"required"`
		APP      string `conf:"required"`
		Env      string `conf:"required"`
		Release  string `conf:"required"`
		Stage    string `conf:"default:provisioning"`
		Mute     bool   `conf:"default:false"`
		Pods     string `conf:""`
		Names    string `conf:""`
		Duration int64  `conf:""`
	}

	cfg.Version.SVN = build
	cfg.Version.Desc = "2020 Bradley McDuffie"

	if err := conf.Parse(os.Args[1:], appname, &cfg); err != nil {
		switch err {
		case conf.ErrHelpWanted:
			usage, err := conf.Usage(appname, &cfg)
			if err != nil {
				return errors.Wrap(err, "generating config usage")
			}
			fmt.Println(usage)
			return nil
		case conf.ErrVersionWanted:
			version, err := conf.VersionString(appname, &cfg)
			if err != nil {
				return errors.Wrap(err, "generating config version")
			}
			fmt.Println(version)
			return nil
		}
		return errors.Wrap(err, "parsing config")
	}

	date := time.Now().Unix() + 1814400 // 3 weeks of seconds

	type response struct {
		Recurrence   string   `json:"recurrence"`
		End          int      `json:"end"`
		MonitorTags  []string `json:"monitor_tags"`
		Canceled     string   `json:"canceled"`
		MonitorID    string   `json:"monitor_id"`
		OrgID        string   `json:"org_id"`
		Disabled     string   `json:"disabled"`
		Start        int      `json:"start"`
		CreatorID    int      `json:"creator_id"`
		ParentID     int      `json:"parent_id"`
		Timezone     string   `json:"timezone"`
		Active       bool     `json:"active"`
		Scope        []string `json:"scope"`
		Message      string   `json:"message"`
		DowntimeType int      `json:"downtime_type"`
		ID           int      `json:"id"`
		UpdaterID    int      `json:"updater_id"`
	}

	if cfg.Duration > 0 {
		seconds := cfg.Duration * 60
		date = time.Now().Unix() + seconds
	}

	var pods []string
	if cfg.Pods != "" {
		pods = strings.Split(cfg.Pods, ",")
	} else if cfg.Names != "" {
		pods = strings.Split(cfg.Names, ",")
	}

	for _, pod := range pods {
		dashedRelease := strings.Replace(cfg.Release, ".", "-", -1)
		if cfg.Mute {
			message := fmt.Sprintf("silenced during %s %s %s", cfg.Release, cfg.Env, cfg.Stage)
			url := fmt.Sprintf("https://api.datadoghq.com/api/v1/downtime?api_key=%s&application_key=%s", cfg.API, cfg.APP)
			var jsonStr string
			if cfg.Pods != "" {
				jsonStr = fmt.Sprintf("{\"scope\": \"environment:%s,pod:%s-%s\",\"end\":\"%d\",\"message\": \"%s\"}", cfg.Env, pod, dashedRelease, date, message)
			} else if cfg.Names != "" {
				jsonStr = fmt.Sprintf("{\"scope\": \"environment:%s,name:%s\",\"end\":\"%d\",\"message\": \"%s\"}", cfg.Env, pod, date, message)
			}
			var payload = []byte(jsonStr)
			req, _ := http.NewRequest("POST", url, bytes.NewBuffer(payload))
			req.Header.Set("Content-Type", "application/json")

			client := &http.Client{}
			resp, err := client.Do(req)
			if err != nil {
				panic(err)
			}
			defer resp.Body.Close()

			body, _ := ioutil.ReadAll(resp.Body)

			res := response{}
			json.Unmarshal(body, &res)

			switch resp.Status {
			case "200 OK":
				color.Green(fmt.Sprintf("%d %s", res.ID, res.Scope))
			default:
				color.Red(fmt.Sprintf("%d %s", res.ID, res.Scope))
			}
		} else {
			url := fmt.Sprintf("https://api.datadoghq.com/api/v1/downtime/cancel/by_scope?api_key=%s&application_key=%s", cfg.API, cfg.APP)
			jsonStr := fmt.Sprintf("{\"scope\": \"environment:%s,pod:%s-%s\"}", cfg.Env, pod, dashedRelease)
			var payload = []byte(jsonStr)
			req, _ := http.NewRequest("POST", url, bytes.NewBuffer(payload))
			req.Header.Set("Content-Type", "application/json")

			client := &http.Client{}
			resp, err := client.Do(req)
			if err != nil {
				panic(err)
			}
			defer resp.Body.Close()

			body, _ := ioutil.ReadAll(resp.Body)
			res := response{}
			json.Unmarshal(body, &res)

			switch resp.Status {
			case "200 OK":
				color.Green(fmt.Sprintf("Downtime canceled for: %s - %s-%s", cfg.Env, pod, dashedRelease))
			default:
				color.Red(fmt.Sprintf("Downtime canceled for: %s - %s-%s", cfg.Env, pod, dashedRelease))
			}
		}
	}

	return nil
}
