module eb-github.com/bradley-mcduffie/dd-downtime

go 1.14

require (
	github.com/ardanlabs/conf v1.3.2
	github.com/fatih/color v1.9.0
	github.com/pkg/errors v0.9.1
)
