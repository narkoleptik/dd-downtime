# dd-downtime - DataDog downtime manager

```
╰─$ dd-downtime -h
Usage: dd-downtime [options] [arguments]

OPTIONS
  --api/$DD_API          <string>  (required)
  --app/$DD_APP          <string>  (required)
  --env/$DD_ENV          <string>  (required)
  --release/$DD_RELEASE  <string>  (required)
  --stage/$DD_STAGE      <string>  (default: provisioning)
  --mute/$DD_MUTE        <bool>    (default: false)
  --pods/$DD_PODS        <string>  (required)
  --help/-h
  display this help message
  --version/-v
  display version information
```
